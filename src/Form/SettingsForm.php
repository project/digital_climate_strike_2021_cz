<?php

namespace Drupal\digital_climate_strike_2021_cz\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class SettingsForm.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'digital_climate_strike_2021_cz.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('digital_climate_strike_2021_cz.settings');
    $form['enable'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable widget on the site'),
      '#default_value' => $config->get('enable'),
    ];
    $form['settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Widget settings'),
      '#states' => [
        //show this textfield only if the radio 'other' is selected above
        'visible' => [
          //don't mistake :input for the type of field. You'll always use
          //:input here, no matter whether your source is a select, radio or checkbox element.
          ':input[name="enable"]' => ['checked' => true],
        ],
      ],
    ];
    $form['settings']['cookie_expiration_day'] = [
      '#type' => 'number',
      '#title' => $this->t('Days before cookie expiration'),
      '#default_value' => $config->get('cookie_expiration_day'),
    ];
    $form['settings']['disable_ga'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Disable Google Analytics usage in the widget'),
      '#default_value' => $config->get('disable_ga'),
    ];
    $form['settings']['always_show_widget'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Always show widget'),
      '#description' => $this->t('Shows widget even when the user closes it. Useful for testing'),
      '#default_value' => $config->get('always_show_widget'),
    ];
    $form['settings']['force_full_page_widget'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Force full page widget'),
      '#description' => $this->t('Forces display of full page widget. Useful for testing'),
      '#default_value' => $config->get('force_full_page_widget'),
    ];
    $form['settings']['show_close_button_on_full_page_widget'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show close button on full page widget.'),
      '#description' => $this->t('Allows user to close the full page widget and visit the site on the day of the strike.'),
      '#default_value' => $config->get('show_close_button_on_full_page_widget'),
    ];
    $form['settings']['footer_display_start_date'] = [
      '#type' => 'date',
      '#title' => $this->t('Start displaying footer widget on:'),
      '#default_value' => $config->get('footer_display_start_date'),
    ];
    $form['settings']['full_page_display_start_date'] = [
      '#type' => 'date',
      '#title' => $this->t('Start displaying full page widget on:'),
      '#default_value' => $config->get('full_page_display_start_date'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('digital_climate_strike_2021_cz.settings')
      ->set('enable', $form_state->getValue('enable'))
      ->set('cookie_expiration_day', $form_state->getValue('cookie_expiration_day'))
      ->set('disable_ga', $form_state->getValue('disable_ga'))
      ->set('always_show_widget', $form_state->getValue('always_show_widget'))
      ->set('force_full_page_widget', $form_state->getValue('force_full_page_widget'))
      ->set('show_close_button_on_full_page_widget', $form_state->getValue('show_close_button_on_full_page_widget'))
      ->set('footer_display_start_date', $form_state->getValue('footer_display_start_date'))
      ->set('full_page_display_start_date', $form_state->getValue('full_page_display_start_date'))
      ->save();
  }

}
