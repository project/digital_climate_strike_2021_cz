var iframeHost = window.location.origin;
var fullPage = window.location.toString().indexOf('fullPage') !== -1;
var showCloseButton = window.location.toString().indexOf('showCloseButton') !== -1;
var language = getUrlParameter('language') ? getUrlParameter('language') : 'cs';
var partnerReferrer = getUrlParameter('partnerReferrer') ? getUrlParameter('partnerReferrer') : null

const moduleSettings = drupalSettings.digital_climate_strike_2021_cz;

var ZAKLIMA_LIVE_OPTIONS = {
  ...(moduleSettings.cookie_expiration_day && {cookieExpirationDays: moduleSettings.cookie_expiration_day}),
  ...(moduleSettings.disable_ga && {disableGoogleAnalytics: moduleSettings.disable_ga}),
  ...(moduleSettings.always_show_widget && {alwaysShowWidget: moduleSettings.always_show_widget}),
  ...(moduleSettings.force_full_page_widget && {forceFullPageWidget: moduleSettings.force_full_page_widget}),
  ...(moduleSettings.show_close_button_on_full_page_widget && {showCloseButtonOnFullPageWidget: moduleSettings.show_close_button_on_full_page_widget}),
  ...(moduleSettings.footer_display_start_date && {footerDisplayStartDate: new Date(moduleSettings.footer_display_start_date * 1000)}),
  ...(moduleSettings.full_page_display_start_date && {fullPageDisplayStartDate: new Date(moduleSettings.full_page_display_start_date * 1000)}),
};

// var ZAKLIMA_LIVE_OPTIONS = {
//   //iframeHost: iframeHost,
//   forceFullPageWidget: fullPage,
//   showCloseButtonOnFullPageWidget: showCloseButton,
//   alwaysShowWidget: true,
//   websiteName: 'Demo',
//   language: language,
//   partnerReferrer: partnerReferrer
// };

function getUrlParameter(name) {
  var url = window.location.href;
  name = name.replace(/[\[\]]/g, '\\$&');
  var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
    results = regex.exec(url);
  if (!results) return null;
  if (!results[2]) return '';
  return decodeURIComponent(results[2].replace(/\+/g, ' '));
}
